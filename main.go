package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"
)

func main() {
	port, err := strconv.Atoi(os.Args[2])
	if err != nil {
		panic(err)
	}
	fmt.Printf("Wait for port %d\n", port)

	for {
		if _, err := net.Dial("tcp", fmt.Sprintf("%s:%d", os.Args[1], port)); err != nil {
			fmt.Println(err)
		} else {
			break
		}
		time.Sleep(time.Second * 3)
	}
	fmt.Println("Port available")
}
